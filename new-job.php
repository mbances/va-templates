<?php
/*
Template Name: New Job
Template Post Type: post, page
*/
get_header(); ?>

<?php $layout_class = shapely_get_layout_class(); ?>
	<div class="row">
		<div class="col-md-3 mb-xs-16 side-navigation-menu"> 
			<div class="site-title-container sidebar-logo">
				<a href="http://videoapplicants:8000/" class="custom-logo-link" rel="home" itemprop="url">
					<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
						$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					?>
					<img width="49" height="50" src="<?php echo $logo[0] ?>" sizes="(max-width: 49px) 100vw, 49px">
				</a>		
			</div>
			<div id="toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</div>
			<div id="popout">
				<?php wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); ?>
			</div>
		</div>
		<div id="primary" class="col-md-9 mb-xs-24">
			<div class="entry-content">

				<h1 class="page-title"><?php single_post_title(); ?></h1>

				<form action="”../customer-details.php”" method="”POST”" name="”customer_details”">
					<div class="row">
						<div class="col-md-3 mb-xs-12">
							<img src="http://videoapplicants:8000/wp-content/themes/shapely-child/img/user.png" alt="John" class="user-avatar" style="width:100%">
							<input type="file" id="fileinput" />
						</div>
						<div class="col-md-9 mb-xs-12">
							Job Title: <input id="job_title" name="job_title" type="text" />
							Job Category: <input id="job_category" name="job_category" type="text" />
							Job Code: <input id="job_code" name="job_code" type="text" />
							Position Type: <input id="position_type" name="position_type" type="text" />
							Job Location: <input id="job_location" name="job_location" type="text" />
							Job Description: <textarea id="job_description" name="job_description" rows="4" cols="50"></textarea>
							Link to Posting on Your Website: <input id="job_link" name="job_link" type="text" />
							<input type="submit" value="Post Job" />
						</div>
					</div>
				</form>

					<script type=”text/javascript”>
					function form_validation() {
					/* Check the Customer Name for blank submission*/
					var customer_name = document.forms[“customer_details”][“customer_name”].value;
					if (customer_name == “” || customer_name == null) {
					alert(“Name field must be filled.”);
					return false;
					}

					/* Check the Customer Email for invalid format */
					var customer_email = document.forms[“customer_details”][“customer_email”].value;
					var at_position = customer_email.indexOf(“@”);
					var dot_position = customer_email.lastIndexOf(“.”);
					if (at_position<1 || dot_position<at_position+2 || dot_position+2>=customer_email.length) {
					alert(“Given email address is not valid.”);
					return false;
					}
					}
					</script>
			</div>
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</div><!-- #primary -->
	</div>
<?php
get_footer();