<?php
/*
Template Name: Matching Jobs
Template Post Type: post, page
*/
get_header(); ?>

<?php $layout_class = shapely_get_layout_class(); ?>
	<div class="row">
	<div class="col-md-3 mb-xs-16 side-navigation-menu"> 
		<div class="site-title-container sidebar-logo">
			<a href="http://videoapplicants:8000/" class="custom-logo-link" rel="home" itemprop="url">
				<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				?>
				<img width="49" height="50" src="<?php echo $logo[0] ?>" sizes="(max-width: 49px) 100vw, 49px">
			</a>		
		</div>
		<div id="toggle">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</div>
		<div id="popout">
			<?php wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); ?>
		</div>
	</div>
		<div id="primary" class="col-md-9 mb-xs-24">
			<div class="entry-content">

				<h1 class="page-title"><?php single_post_title(); ?></h1>

				<div class="row">

					<div class="col-md-4 mb-xs-12">
						
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<a href="/">View Job</a>
						</div>
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<a href="/">View Job</a>
						</div>
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<a href="/">View Job</a>
						</div> 
					</div>
					<div class="col-md-8 mb-xs-12">
						<div class="job-full">
							<h3 class="job-title">
								Mechanical Engineer
							</h3>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-salary">
								Salary Range: $75,000 to $90,000
							</div>

							<div class="job-description">
								<div>Description:</div>
								<div>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								 Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								 Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								</div>
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<a href="/">Send My Profile to Employer</a>
						</div>
					</div>

				</div>

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div>
		</div><!-- #primary -->
	</div>
<?php
get_footer();