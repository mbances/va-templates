<?php
/*
Template Name: Front Page
Template Post Type: post, page
*/
get_header(); ?>

<?php $layout_class = shapely_get_layout_class(); ?>
	<div class="row">
	<div class="col-md-3 mb-xs-16 side-navigation-menu"> 
		<div class="site-title-container sidebar-logo">
				<a href="/" class="custom-logo-link" rel="home" itemprop="url">
					<img width="49" height="50" src="http://videoapplicants:8000/wp-content/uploads/2018/04/cropped-videoapplicant-logo-2-150x150.png" class="custom-logo logo" alt="Video Applicant" itemprop="logo" srcset="http://videoapplicants:8000/wp-content/uploads/2018/04/cropped-videoapplicant-logo-2-150x150.png 150w, http://videoapplicants:8000/wp-content/uploads/2018/04/cropped-videoapplicant-logo-2-294x300.png 294w, http://videoapplicants:8000/wp-content/uploads/2018/04/cropped-videoapplicant-logo-2-768x785.png 768w, http://videoapplicants:8000/wp-content/uploads/2018/04/cropped-videoapplicant-logo-2-1002x1024.png 1002w" sizes="(max-width: 49px) 100vw, 49px">
				</a>		
		</div>
		<div id="toggle">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</div>
		<div id="popout">
			<?php wp_nav_menu( array( 'theme_location' => 'side-menu', 'menu_class' => 'side-nav' ) ); ?>
		</div>

	</div>

		<div id="primary" class="entry-content">
			<div class="row">
				<div class="col-md-9">
					<p class="site-name"> A professional platform for personal career <br />
						career and occupational ads, mentorship forum and jobs.
					</p>
				</div>
				<div class="col-md-3 home-section">
					
					<div class="user-section">
						<button class="user-type">USERS</button>
						<p class="description">
							Post your career and occupation ads video, pictures or qualifications.
						</p>

						<div class="user-button">
							<button>Sign Up</button>

							<button>Login</button>
						</div>
					</div>
				</div>

				<div class="col-md-3 home-section">
					
					<div class="user-section">
						<button class="user-type">Search Here for Applicants</button>
					</div>

						<p class="description"></p>

					<div class="user-button">
						<button class="search-jobs">Search for Jobs</button>
					</div>
				</div>

				<div class="col-md-3 home-section">
					
					<div class="user-section">
						<button class="user-type">Employers</button>
						<p class="description">
							Post a job or project.
						</p>
					</div>

					<div class="user-button">
						<button>Sign Up</button>

						<button>Login</button>
					</div>
				</div>
			</div>
		</div><!-- primary -->
	</div>
<?php
get_footer();
