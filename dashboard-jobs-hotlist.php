<?php
/*
Template Name: Dashboard Jobs Hotlist
Template Post Type: post, page
*/
get_header(); ?>

<?php $layout_class = shapely_get_layout_class(); ?>
	<div class="row">
	<div class="col-md-3 mb-xs-16 side-navigation-menu"> 
		<div class="site-title-container sidebar-logo">
			<a href="http://videoapplicants:8000/" class="custom-logo-link" rel="home" itemprop="url">
				<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				?>
				<img width="49" height="50" src="<?php echo $logo[0] ?>" sizes="(max-width: 49px) 100vw, 49px">
			</a>		
		</div>
		<div id="toggle">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</div>
		<div id="popout">
			<?php wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); ?>
		</div>
	</div>
		<div id="primary" class="col-md-9 mb-xs-24">
			<div class="entry-content">

				<h1 class="page-title"><?php single_post_title(); ?></h1>

				<div class="row">
					<div class="col-md-4 mb-xs-12">
						<div class="applicant-preview">
							<h4 class="applicant-preview-title">Jon Doe</h4>
						</div>
						<div class="applicant-preview">
							<h4 class="applicant-preview-title">Alice Smythe</h4>
						</div>
						<div class="applicant-preview">
							<h4 class="applicant-preview-title">Jon Doe</h4>
						</div>
						<div class="applicant-preview">
							<h4 class="applicant-preview-title">Alice Smythe</h4>
						</div>
					</div>
					<div class="col-md-8 mb-xs-12">
						<div class="applicant-full">
							<div class="applicant-hotlist-name">
								<h3>Alice Smythe</h3>
							</div>
							<div class="applicant-hotlist-phone">
								Phone: 250 234 1577
							</div>
							<div class="applicant-hotlist-email">
								Email: asmythe@hotmail.com
							</div>
							<div class="applicant-hotlist-job-category">
								Job Category: Engineering
							</div>

							<a href="/">Go To Alice's Profile</a>
						</div>
					</div>	
				</div>

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div>
		</div><!-- #primary -->
	</div>
<?php
get_footer();