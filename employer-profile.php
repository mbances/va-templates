<?php
/*
Template Name: Employer Profile
Template Post Type: post, page
*/
get_header(); ?>

<?php $layout_class = shapely_get_layout_class(); ?>
	<div class="row">
	<div class="col-md-3 mb-xs-16 side-navigation-menu"> 
		<div class="site-title-container sidebar-logo">
			<a href="http://videoapplicants:8000/" class="custom-logo-link" rel="home" itemprop="url">
				<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				?>
				<img width="49" height="50" src="<?php echo $logo[0] ?>" sizes="(max-width: 49px) 100vw, 49px">
			</a>		
		</div>
		<div id="toggle">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</div>
		<div id="popout">
			<?php wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); ?>
		</div>
	</div>
		<div id="primary" class="col-md-9 mb-xs-24">

			<div class="entry-content">

				<h1 class="page-title"><?php single_post_title(); ?></h1>

				<form action="”../customer-details.php”" method="”POST”" name="”customer_details”">
					<div class="row">
						<div class="col-md-3 mb-xs-12">
							<img src="http://videoapplicants:8000/wp-content/themes/shapely-child/img/user.png" alt="John" class="user-avatar" style="width:100%">
							<input type="file" id="fileinput" />
						</div>
						<div class="col-md-9 mb-xs-12">
							Company Name: <input id="company_name" name="company_name" type="text" />
							Operating Name: <input id="operating_name" name="operating_name" type="text" />
							Business Number: <input id="business_number" name="business_number" type="text" />
							Company Address: <input id="company_address" name="company_address" type="text" />
							Company Contact: <input id="company_contact" name="company_contact" type="text" />
							Contact Email: <input id="contact_email" name="contact_email" type="text" />
							Contact Phone: <input id="contact_phone" name="contact_phone" type="text" />
							Company Website: <input id="company_website" name="company_website" type="text" />
							<input type="submit" value="Next Step" />
						</div>
					</div>
				</form>
				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div>
		</div><!-- #primary -->
	</div>
<?php
get_footer();