<?php
/*
Template Name: Dashboard Jobs
Template Post Type: post, page
*/
get_header(); ?>

<?php $layout_class = shapely_get_layout_class(); ?>
	<div class="row">
	<div class="col-md-3 mb-xs-16 side-navigation-menu"> 
		<div class="site-title-container sidebar-logo">
			<a href="http://videoapplicants:8000/" class="custom-logo-link" rel="home" itemprop="url">
				<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				?>
				<img width="49" height="50" src="<?php echo $logo[0] ?>" sizes="(max-width: 49px) 100vw, 49px">
			</a>		
		</div>
		<div id="toggle">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</div>
		<div id="popout">
			<?php wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); ?>
		</div>
	</div>
		<div id="primary" class="col-md-9 mb-xs-24">
			<div class="entry-content">

				<h1 class="page-title"><?php single_post_title(); ?></h1>

				<div class="row">

					<div class="col-md-6 mb-xs-12">

						<h3 class="page-section-title">Current Posted Jobs</h3>
						
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<button class="job-description">
								<a href="/">Go To Job Description</a>
							</button>
						</div>
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<button class="job-description">
								<a href="/">Go To Job Description</a>
							</button>						</div>
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<button class="job-description">
								<a href="/">Go To Job Description</a>
							</button>
						</div>

						<h3 class="page-section-title">Former Job Postings</h3>
						
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<button class="job-description">
								<a href="/">Go To Job Description</a>
							</button>
						</div>
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<button class="job-description">
								<a href="/">Go To Job Description</a>
							</button>
						</div>
						<div class="job-preview">
							<h4 class="job-title">
								Mechanical Engineer
							</h4>
							<div class="job-location">
								Burnaby, BC
							</div>

							<div class="job-views">
								Views: 35
							</div>
							<div class="job-possible-matches">
								Possible Matches: 130
							</div>

							<div class="job-expiry-date">
								Expiry Date: July 30, 2018
							</div>

							<button class="job-description">
								<a href="/">Go To Job Description</a>
							</button>						
						</div>
					</div>

					<div class="col-md-6 mb-xs-12">
						<h3 class="page-section-title">Applicant Hotlist</h3>
						<div class="row">
							<div class="col-md-4 mb-xs-12">
								<ul>
									<li>Jon Doe</li>
									<li>Alice Smythe</li>
									<li>Jon Doe</li>
									<li>Alice Smythe</li>
									<li>Jon Doe</li>
								</ul>
							</div>
							<div class="col-md-8 mb-xs-12">
								<div class="applicant-hotlist-name">
									Alice Smythe
								</div>
								<div class="applicant-hotlist-phone">
									Phone: 250 234 1577
								</div>
								<div class="applicant-hotlist-email">
									Email: asmythe@hotmail.com
								</div>
								<div class="applicant-hotlist-job-category">
									Job Category: Engineering
								</div>

							<button class="job-description">
								<a href="/">Go To Job Description</a>
							</button>							
						</div>
						</div>
					</div>
				</div>

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div>
		</div><!-- #primary -->
	</div>
<?php
get_footer();