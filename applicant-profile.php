<?php
/*
Template Name: Applicant Profile
Template Post Type: post, page
*/
get_header(); ?>

<?php $layout_class = shapely_get_layout_class(); ?>
	<div class="row">
		<div class="col-md-3 mb-xs-16 side-navigation-menu"> 
			<div class="site-title-container sidebar-logo">
				<a href="/" class="custom-logo-link" rel="home" itemprop="url">
					<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
						$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					?>
					<img width="49" height="50" src="<?php echo $logo[0] ?>" sizes="(max-width: 49px) 100vw, 49px">
				</a>		
			</div>
			<div id="toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</div>
			<div id="popout">
				<?php wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); ?>
			</div>
		</div>
		<div id="primary" class="col-md-9 mb-xs-24">
			<div class="content">
				<div class="row profile-header">
					<h1 class="profile-name">John Doe</h1>
					<div class="row">
						<div class="col-md-4 mb-xs-12">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/user.png" alt="John" class="user-avatar" style="width:100%">
						</div>

						<div class="col-md-8 mb-xs-12">
							<div class="col-md-4 mb-xs-12 user-information">
								<span class="profile-address">
									1234 Somewhere St
									Anywhere, BC
									A1A 1A1
								</span>
								<span class="profile-email">jondoe@hotmail.com</span>
								<span class="profile-phone">250 111 1111</span>
							</div>
							<div class="col-md-4 mb-xs-12 user-information">
								<span class="profile-job-type">Job Type (s): Full Time, Part Time</span>
								<span class="profile-job-categories">Job Categories: Engineering, Science</span>
							</div>

								<div class="col-md-8">
									<div class="user-bio">
										Hello, I'm John and I'm a recent graduate of the Civil Engineering Program at
										the University of Victoria...
									</div>
								</div>
						</div>


					</div><!-- col-md-8 -->
				</div><!-- profile-header -->



				<div class="row links-row">
					<div class="col-md-4 mb-xs-12">
						<div class="profile-resume">
							<div class="icon-large">
								<i class="far fa-file-alt"></i>
							</div>
							<div class="icon-link">
								<a href="/">Resume</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 mb-xs-12">
						<div class="profile-video-cover">
							<div class="icon-large">
								<i class="far fa-file-video"></i>
							</div>
							<div class="icon-link">
								<a href="/">Video Cover Letter</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 mb-xs-12">
						<div class="profile-linkedin">
							<div class="icon-large">
								<i class="fab fa-linkedin-in"></i>
							</div>
							<div class="icon-link">
								<a href="/">Linkedin Profile</a>
							</div>
						</div>
					</div>
				</div>

				<div class="row profile-videos">
					<div class="col-md-4 mb-xs-12">
						<p> Upload </p>

						<div class="col-md-4 mb-xs-12">
							<div class="profile-video-selector">
								<div class="profile-video-link">
									<a href="/">Videos</a>
								</div>
							</div>
						</div>

						<div class="col-md-4 mb-xs-12">
							<div class="profile-video-selector">
								<div class="profile-video-link">
									<a href="/">Links</a>
								</div>
							</div>
						</div>
					</div>


					<div class="col-md-8 mb-xs-12">
						<div class="profile-video-item active">
							<video id="gum" src="http://media.w3.org/2010/05/sintel/trailer.mp4" controls></video>
						</div>
						<div class="profile-video-item">
							<video id="gum" src="http://media.w3.org/2010/05/sintel/trailer.mp4" controls></video>
						</div>
						<div class="profile-video-item">
							<video id="gum" src="http://media.w3.org/2010/05/sintel/trailer.mp4" controls></video>
						</div>
						<div class="profile-video-item">
							<video id="gum" src="http://media.w3.org/2010/05/sintel/trailer.mp4" controls></video>
						</div>
						<div class="profile-video-item">
							<video id="gum" src="http://media.w3.org/2010/05/sintel/trailer.mp4" controls></video>
						</div>
					</div>
				</div>

				<div class="row navigate-pages">
					<div class="col-md-6 mb-xs-12">
						<div class="page-nav-left">
							<a href="/">< Back to Edit Profile</a>
						</div>
					</div>
					<div class="col-md-6 mb-xs-12">
						<div class="page-nav-right">
							<a href="/">Search Posted Jobs ></a>
						</div>
					</div>
				</div>

				<?php
				// while ( have_posts() ) :
				// 	the_post();

				// 	get_template_part( 'template-parts/content' );

				// 	// If comments are open or we have at least one comment, load up the comment template.
				// 	if ( comments_open() || get_comments_number() ) :
				// 		comments_template();
				// 	endif;

				// endwhile; // End of the loop.
				?>
			</div>
		</div><!-- #primary -->
	</div>
<?php
get_footer();